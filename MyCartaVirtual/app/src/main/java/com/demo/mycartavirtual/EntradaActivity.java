package com.demo.mycartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView;
import android.view.View;

public class EntradaActivity extends Activity {
    String [] productos = new String[]{"Frijoles","Mondongo","Sancocho","Sudado","Guineo"};
    private String[] precios = {"$10.000","$12.000","$10.000","$9.000","$12.000"};
    private TextView tv1;
    private ListView lv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada);

        tv1 = (TextView)findViewById(R.id.txtPrecios);
        lv1 = (ListView)findViewById(R.id.lista);
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, productos);
        lv1.setAdapter(adapter);

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                tv1.setText("El precio de "+ lv1.getItemAtPosition(i) + " es: "+ precios[i]);
            }
        });
    }
}
