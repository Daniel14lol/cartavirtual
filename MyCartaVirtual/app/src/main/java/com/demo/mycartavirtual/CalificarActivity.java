package com.demo.mycartavirtual;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

public class CalificarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calificar);
    }
    void limpiar(View v){
        EditText tv1 = (EditText)findViewById(R.id.txtComentario);
        EditText tv2 = (EditText)findViewById(R.id.txtNombre);
        EditText tv3 = (EditText)findViewById(R.id.txtCorreo);
        RatingBar rat = (RatingBar)findViewById(R.id.calificacion);

        if(tv1.getText().equals("") && tv2.getText().equals("") && tv3.getText().equals("")){
            Toast.makeText(getApplicationContext(),"¡Por favor ponga su nombre, correo y calificación!",Toast.LENGTH_LONG).show();
        }else {
            tv1.setText("Comentarios");
            tv2.setText("Nombre");
            tv3.setText("Correo");
            rat.setRating(0);
            Toast.makeText(getApplicationContext(), "¡Gracias por su comentario y calificación!", Toast.LENGTH_LONG).show();
        }
    }
}
