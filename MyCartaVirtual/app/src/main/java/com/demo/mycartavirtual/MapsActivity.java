package com.demo.mycartavirtual;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sede = new LatLng(6.240393, -75.589300);
        LatLng pos1 = new LatLng(6.241913,  -75.589638);
        LatLng pos2 = new LatLng(6.243310, -75.590518);
        LatLng pos3 = new LatLng(6.240281, -75.589606);
        LatLng pos4 = new LatLng(6.244696, -75.589531);
        mMap.addMarker(new MarkerOptions().position(sede).title("UPB"));
        mMap.addMarker(new MarkerOptions().position(pos1).title("Restaurante 1"));
        mMap.addMarker(new MarkerOptions().position(pos2).title("Restaurante 2"));
        mMap.addMarker(new MarkerOptions().position(pos3).title("Restaurante 3"));
        mMap.addMarker(new MarkerOptions().position(pos4).title("Restaurante 4"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sede,14));
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }
}
