package com.demo.mycartavirtual;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class PlatoActivity extends AppCompatActivity {

    String[] productos = new String[]{"Arroz con pollo", "Arroz con carne", "Arroz con pescado", "Arroz con papas", "Ensalada"};
    private String[] precios = {"$16.000","$13.000","$15.000","$11.000","$6.000"};
    private TextView tv1;
    private ListView lv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada);

        tv1 = (TextView)findViewById(R.id.txtPrecios);
        lv1 = (ListView)findViewById(R.id.lista);
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, productos);
        lv1.setAdapter(adapter);

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                tv1.setText("El precio de "+ lv1.getItemAtPosition(i) + " es: "+ precios[i]);
            }
        });
    }
}
