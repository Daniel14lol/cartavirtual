package com.demo.mycartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class PostreActivity extends Activity {
    String[] productos = new String[]{"Pastel de arequipe", "Pastel de guayaba", "Torta de chocolate", "Muffin", "Flan de caramelo", "Cheesecake"};
    private String[] precios = {"$2.000","$2.000","$3.000","$5.000","$6.000","$7.000"};
    private TextView tv1;
    private ListView lv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postre);

        tv1 = (TextView)findViewById(R.id.txtPrecios);
        lv1 = (ListView)findViewById(R.id.lista);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, productos);
        lv1.setAdapter(adapter);

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                tv1.setText("El precio de "+ lv1.getItemAtPosition(i) + " es: "+ precios[i]);
            }
        });
    }
}
