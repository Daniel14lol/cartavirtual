package com.demo.mycartavirtual;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class BebidaActivity extends Activity {
    String[] productos = new String[]{"Gaseosa", "Lulo", "Maracuya", "Guanábana", "Guandolo", "Banano"};
    private String[] precios = {"$2.000","$3.000","$3.000","$3.500","$2.500"};
    private TextView tv1;
    private ListView lv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bebida);

        tv1 = (TextView)findViewById(R.id.txtPrecios);
        lv1 = (ListView)findViewById(R.id.lista);
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, productos);
        lv1.setAdapter(adapter);

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                tv1.setText("El precio de "+ lv1.getItemAtPosition(i) + " es: "+ precios[i]);
            }
        });
    }
}